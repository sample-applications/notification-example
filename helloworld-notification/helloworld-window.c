/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gtk/gtk.h>
#include "helloworld-notification.h"
#include "helloworld-window.h"

#define RESOURCE_PREFIX "/org/apertis/HelloWorld/Notification"
#define WIN_WIDTH  480
#define WIN_HEIGHT 800

struct _HlwAppWindow
{
  GtkApplicationWindow parent;
  GtkWidget *window, *button, *button_box;
};

G_DEFINE_TYPE(HlwAppWindow, hlw_app_window, GTK_TYPE_APPLICATION_WINDOW);

static GBytes *
load_icon_bytes (void)
{
  g_autoptr(GError) err = NULL;
  GBytes *data = g_resources_lookup_data (
    RESOURCE_PREFIX "/icons/notification_icon.png",
    G_RESOURCE_LOOKUP_FLAGS_NONE, &err);

  if (data == NULL)
  {
    g_error ("Data lookup failed: %s", err->message);
    return NULL;
  }
  return data;
}

static gboolean
notification_timeout_cb (gpointer app)
{
  g_application_withdraw_notification (G_APPLICATION (app), "example");

  return G_SOURCE_REMOVE;
}

static void
notify (GApplication *app)
{
  g_autoptr (GNotification) notification = NULL;
  g_autoptr (GIcon) icon = NULL;
  g_autoptr (GBytes) bytes = NULL;

  notification = g_notification_new ("Time to notify");
  g_notification_set_body (notification, "This is a helloworld notification");

  bytes = load_icon_bytes ();
  icon = g_bytes_icon_new (bytes);

  g_notification_set_icon (notification, icon);
  g_application_send_notification (G_APPLICATION (app), "example", notification);
   
  g_timeout_add_seconds (2, notification_timeout_cb, app);
}

static void
button_pressed_cb (GtkWidget *button,
                   gpointer user_data)
{
  GApplication *app = g_application_get_default ();
  notify (app);
}

static void
hlw_app_window_init (HlwAppWindow *app)
{
  HlwAppWindow *win = HLW_APP_WINDOW (app);
  gtk_window_set_default_size (GTK_WINDOW (win),WIN_WIDTH,WIN_HEIGHT);
  win->button = gtk_button_new_with_label ("Click to Notify");
  g_signal_connect (win->button,"clicked",G_CALLBACK(button_pressed_cb),win);
  win->button_box = gtk_button_box_new (GTK_ORIENTATION_HORIZONTAL);
  gtk_container_add (GTK_CONTAINER(win->button_box), GTK_WIDGET(win->button));
  gtk_container_add (GTK_CONTAINER(win), GTK_WIDGET(win->button_box));
  gtk_widget_show_all (GTK_WIDGET(win));
}

static void
hlw_app_window_class_init (HlwAppWindowClass *klass)
{
}

HlwAppWindow *
hlw_app_window_new (HlwApp *app)
{
  return g_object_new (HLW_APP_WINDOW_TYPE, "application", app, NULL);
}
